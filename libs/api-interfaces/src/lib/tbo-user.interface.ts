import { Document } from 'mongoose';

// Generated by https://quicktype.io

export interface TBOUser extends Document {
    Status: number;
    TokenId: string;
    Error: TBOError;
    Member: TBOMember;
}

export interface TBOError {
    ErrorCode: number;
    ErrorMessage: string;
}

export interface TBOMember {
    FirstName: string;
    LastName: string;
    Email: string;
    MemberId: number;
    AgencyId: number;
    LoginName: string;
    LoginDetails: string;
    isPrimaryAgent: boolean;
}
