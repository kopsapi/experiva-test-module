export interface TBOFlightsFilter {
    departureTime: ('Morning' | 'Afternoon' | 'Evening' | 'Night')[],
    refundable: (true | false)[],
    stops: (0 | 1 | 2)[],
    airlines: string[]
}