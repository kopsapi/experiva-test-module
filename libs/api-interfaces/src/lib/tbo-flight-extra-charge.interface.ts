import { Document } from 'mongoose';

export interface ITBOFlightExtraCharge extends Document {
  baseMargin: number;
  gst: number;
}
