export interface TBOHotelSearchRequest {
  EndUserIp: string;
  TokenId: string;
  CheckInDate: string;
  NoOfNights: number;
  CountryCode: string;
  CityId: number;
  PreferredCurrency: string;
  GuestNationality: string;
  NoOfRooms: number;
  RoomGuests: RoomGuest[];
  MaxRating: number;
  MinRating: number;
}

export interface RoomGuest {
  NoOfAdults: number;
  NoOfChild: number;
  ChildAge: number[];
}
