// Generated by https://quicktype.io

export interface Airport {
    name: string;
    city: string;
    country: string;
    iata: string;

    AirportCode?: string;
    AirportName?: string;
    CityCode?: string;
    CityName?: string;
    CountryCode?: string;
    CountryName?: string;
    Terminal?: string;
}
