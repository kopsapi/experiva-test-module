import { Document } from 'mongoose';
export interface IGreeting extends Document {
    greeting: string;
    createdBy: string;
    modifiedBy: string;
    status: number;
    createdAt: string;
    modifiedAt: string;
}