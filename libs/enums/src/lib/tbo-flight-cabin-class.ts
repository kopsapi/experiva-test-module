export enum TBOFlightCabinClass {
    All = '1',
    Economy = '2',
    PremiumEconomy = '3',
    Business = '4',
    PremiumBusiness = '5',
    First = '6'
}