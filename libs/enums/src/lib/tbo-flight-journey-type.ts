export enum TBOFlightJourneyType {
    OneWay = '1',
    Return = '2',
    MultiStop = '3',
    AdvanceSearch = '4',
    SpecialReturn = '5'
}