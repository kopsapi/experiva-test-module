import { Schema, Model, Document } from 'mongoose';
import { Injectable } from '@nestjs/common';

@Injectable()
export class DBService<T extends Document> {

  model(model: Model<T>) {
    return new MongoWrapper(model);
  }
}

class MongoWrapper<T extends Document> {
  constructor(private readonly model: Model<T>) { }

  find(conditions: any) {
    return this.model.find(conditions);
  }

  findOne(conditions: any) {
    return this.model.findOne(conditions);
  }

  findById(id: string) {
    return this.model.findById(id);
  }

  create(obj: any) {
    return this.model.create(obj);
  }

  update(conditions: any, doc: any) {
    return this.model.updateOne(conditions, doc);
  }

  delete(conditions: any) {
    return this.model.deleteOne(conditions);
  }

  deleteById(id: string) {
    return this.model.findByIdAndDelete(id);
  }
}
