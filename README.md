## Installing dependencies

To install dependencies run `npm i`

## Run the project

Mongodb must be running on the machine before starting the API

To run the project one has to start all the required apps including:

- API: `ng serve api --port 7777`
- Experiva: `ng serve -o`
- [Others if required]

## About NX

[Refer this doc](./README-NX.md)
