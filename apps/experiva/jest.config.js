module.exports = {
  name: 'experiva',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/experiva',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
