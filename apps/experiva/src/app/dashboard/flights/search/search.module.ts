import { NgModule } from '@angular/core';
import { SearchComponent } from './search.component';
import { SharedModule } from '../../../shared/shared.module';
import { MaterialModule } from '../../../material/material.module';
import { RouterModule } from '@angular/router';
import { OneWayFlightsSearchComponent } from './one-way-flights-search/one-way-flights-search.component';
import { ReturnFlightsSearchComponent } from './return-flights-search/return-flights-search.component';
import { FilterFlights } from './filter-flights.pipe';
import { OneWayFlightsSearchMobileFilter } from './one-way-flights-search-mobile-filter/one-way-flights-search-mobile-filter.component';
import { MultiStepFlightsSearchComponent } from './multi-step-flights-search/multi-step-flights-search.component';

@NgModule({
  declarations: [
    SearchComponent,
    OneWayFlightsSearchComponent,
    ReturnFlightsSearchComponent,
    FilterFlights,
    OneWayFlightsSearchMobileFilter,
    MultiStepFlightsSearchComponent
  ],
  providers: [FilterFlights],
  imports: [
    SharedModule,
    MaterialModule,
    RouterModule.forChild([
      {
        path: '',
        component: SearchComponent
      }
    ])
  ]
})
export class SearchModule {}
