import { Component } from '@angular/core';

@Component({
  selector: 'one-way-flights-search-mobile-filter',
  templateUrl: './one-way-flights-search-mobile-filter.component.html',
  styleUrls: ['./one-way-flights-search-mobile-filter.component.scss']
})
export class OneWayFlightsSearchMobileFilter {}
