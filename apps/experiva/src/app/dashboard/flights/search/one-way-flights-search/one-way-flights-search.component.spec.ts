import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OneWayFlightsSearchComponent } from './one-way-flights-search.component';

describe('OneWayFlightsSearchComponent', () => {
  let component: OneWayFlightsSearchComponent;
  let fixture: ComponentFixture<OneWayFlightsSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OneWayFlightsSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OneWayFlightsSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
