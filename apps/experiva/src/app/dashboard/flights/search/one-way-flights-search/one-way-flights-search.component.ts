import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  Input,
  ChangeDetectionStrategy,
  OnChanges,
  SimpleChanges,
  ElementRef,
  ViewChild,
  ContentChild,
  TemplateRef,
  Inject
} from '@angular/core';
import { TBOFlight, Airline, TBOFlightsFilter } from '@experiva/api-interfaces';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { StorageService } from 'apps/experiva/src/app/core/services/storage/storage.service';
import { SESSION_STORAGE } from 'apps/experiva/src/app/core/services/storage/session-storage.token';
import { SessionStorageKeys } from 'apps/experiva/src/app/shared/constants/session-storage-keys';

@Component({
  selector: 'experiva-one-way-flights-search',
  templateUrl: './one-way-flights-search.component.html',
  styleUrls: ['./one-way-flights-search.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OneWayFlightsSearchComponent implements OnInit, OnChanges {
  airlinesAvailable: Map<string, Airline> = new Map<string, Airline>();
  moment = moment;
  _filters: {
    departureTime: ('Morning' | 'Afternoon' | 'Evening' | 'Night')[];
    refundable: (true | false)[];
    stops: (0 | 1 | 2)[];
    airlines: string[];
  } = {
    departureTime: [],
    refundable: [],
    stops: [],
    airlines: []
  };
  constructor(
    private readonly router: Router,
    @Inject(SESSION_STORAGE) private readonly sessionStorage: StorageService
  ) {
    // clear current flight selection
    this.sessionStorage.delete(SessionStorageKeys.selectedFlights);
  }
  @Output() filters: EventEmitter<TBOFlightsFilter[]> = new EventEmitter();

  @Input() loading: boolean;

  @Input() flights: TBOFlight[][];
  @Input() availableAirlines: Airline[][];

  @ViewChild('filtersEl', { static: true }) filtersEl: ElementRef<
    HTMLDivElement
  >;
  ngOnChanges(changes: SimpleChanges): void {
    if (
      changes['availableAirlines'] &&
      changes['availableAirlines'].currentValue
    ) {
      const airlines = changes['availableAirlines']
        .currentValue[0] as Airline[];
      this.airlinesAvailable = airlines.reduce((acc, airline) => {
        acc.set(airline.AirlineCode, airline);
        return acc;
      }, new Map<string, Airline>());
    }
  }

  ngOnInit() {
    this.filters.emit([this._filters]);
  }

  filterDepartureTime(
    time: 'Morning' | 'Afternoon' | 'Evening' | 'Night',
    checked: boolean
  ) {
    const timeIndex = this._filters.departureTime.indexOf(time);
    if (timeIndex >= 0) {
      if (!checked) {
        this._filters.departureTime.splice(timeIndex, 1);
      }
    } else {
      if (checked) {
        this._filters.departureTime.push(time);
      }
    }
    this.filters.emit([this._filters]);
  }

  filterRefundableFlights(isRefundable: boolean, checked: boolean) {
    const index = isRefundable
      ? this._filters.refundable.indexOf(true)
      : this._filters.refundable.indexOf(false);
    if (checked && index === -1) {
      this._filters.refundable.push(isRefundable);
    } else if (!checked && index >= 0) {
      this._filters.refundable.splice(index, 1);
    }
    this.filters.emit([this._filters]);
  }

  filterFilghtsByStops(stopCount: 0 | 1 | 2, checked: boolean) {
    const index = this._filters.stops.indexOf(stopCount);
    if (checked && index === -1) {
      this._filters.stops.push(stopCount);
    } else if (!checked && index >= 0) {
      this._filters.stops.splice(index, 1);
    }
    this.filters.emit([this._filters]);
  }
  filterFlightsByAirline(airlineCode: string, checked: boolean) {
    const index = this._filters.airlines.indexOf(airlineCode);
    if (checked && index === -1) {
      this._filters.airlines.push(airlineCode);
    } else if (!checked && index >= 0) {
      this._filters.airlines.splice(index, 1);
    }
    this.filters.emit([this._filters]);
  }

  openFilters() {
    this.filtersEl.nativeElement.classList.add('visible');
  }

  review(flight: TBOFlight) {
    this.sessionStorage.set(SessionStorageKeys.selectedFlights, [flight]);
    this.router.navigate(['/dashboard/flights/review/onwards']);
  }
}
