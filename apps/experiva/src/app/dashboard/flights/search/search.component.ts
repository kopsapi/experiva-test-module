import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { FormGroup } from '@angular/forms';
import { TBOFlightJourneyType, TBOFlightCabinClass } from '@experiva/enums';
import { Observable, combineLatest, Subject, BehaviorSubject, of } from 'rxjs';
import {
  Airport,
  TBOFligthsQuery,
  TBOFlightQuerySegment,
  TBOFlight,
  Airline,
  TBOFlightsFilter
} from '@experiva/api-interfaces';
import { ariportsSelector } from '../../dashboard.selectors';
import {
  map,
  filter,
  tap,
  catchError,
  switchMap,
  startWith
} from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { DashboardState } from '../../reducers/dashboard.reducers';
import { ApiService } from '../../../core/services/http/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FilterFlights } from './filter-flights.pipe';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { OneWayFlightsSearchMobileFilter } from './one-way-flights-search-mobile-filter/one-way-flights-search-mobile-filter.component';
import { FlightSearchService } from '../../flights-search.service';
@Component({
  selector: 'experiva-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  TBOFlightJourneyType = TBOFlightJourneyType;
  loading = false;
  moment = moment;
  today = new Date();
  availableAirlines: Airline[][] = [[]];
  _flightSearchQuery: TBOFligthsQuery;
  selectedJourneyType: TBOFlightJourneyType = TBOFlightJourneyType.OneWay;
  flightSearchForm: FormGroup;
  filters$: Subject<TBOFlightsFilter[]> = new Subject();
  loadFlights$ = new BehaviorSubject(true);
  journeyTypes = [
    {
      label: 'One Way',
      value: TBOFlightJourneyType.OneWay
    },

    {
      label: 'Return',
      value: TBOFlightJourneyType.Return
    },

    {
      label: 'Multi Stop',
      value: TBOFlightJourneyType.MultiStop
    }
  ];
  _airports: Airport[] = [];
  airports$: Observable<Airport[]>;

  flights$: Observable<TBOFlight[][]> = combineLatest(
    this.loadFlights$.pipe(
      filter(() => this.flightSearchForm.valid),
      tap(() => {
        this.loading = true;
      }),
      switchMap(() => this.flightSearchService.flights$(this.flightSearchForm)),
      tap(res => {
        this.loading = false;
      }),
      tap(trips => {
        this.availableAirlines = [
          trips[0]
            .map(flightOption => flightOption.Segments)
            .map(segments => segments[0][0])
            .map(flight => flight.Airline),
          trips.length > 1
            ? trips[0]
                .map(flightOption => flightOption.Segments)
                .map(segments => segments[0][0])
                .map(flight => flight.Airline)
            : []
        ];
      }),
      catchError(err => {
        this.loading = false;
        return of([]);
      })
    ),
    this.filters$.pipe(startWith([]))
  ).pipe(
    map(([trips, filters]) => {
      const flights = [
        this.flightsFilter.transform(trips[0], filters[0]),
        this.flightsFilter.transform(trips[1], filters[1])
      ];

      return flights;
    })
  );

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly flightSearchService: FlightSearchService,
    private readonly store: Store<DashboardState>,
    private readonly api: ApiService,
    private readonly flightsFilter: FilterFlights,
    private readonly _bottomSheet: MatBottomSheet
  ) {
    const _flightSearchQuery = {} as TBOFligthsQuery;
    this.route.snapshot.queryParamMap.keys.forEach(key => {
      _flightSearchQuery[key] = this.route.snapshot.queryParamMap.get(key);
    });
    if (
      _flightSearchQuery.Segments &&
      typeof _flightSearchQuery.Segments === typeof ''
    ) {
      _flightSearchQuery.Segments = JSON.parse(
        _flightSearchQuery.Segments as any
      );
    }
    this._flightSearchQuery = _flightSearchQuery;
    this.airports$ = this.store.select(ariportsSelector);
    this.mapToAirportName = this.mapToAirportName.bind(this);
    this.airports$.subscribe(airports => {
      this._airports = airports || [];
      this.flightSearchForm = this.flightSearchService.getFreshForm(
        this._flightSearchQuery
      );
      this.selectedJourneyType = this._flightSearchQuery.JourneyType
        ? this._flightSearchQuery.JourneyType
        : TBOFlightJourneyType.OneWay;
    });
  }

  addQuerySegment() {
    this.flightSearchService.addQuerySegment(this.flightSearchForm);
  }

  filterAirports(airports: Airport[], searchString: string): Airport[] {
    let filteredAirports = [];
    if (airports && searchString && typeof searchString === typeof '') {
      const filterValue = (searchString || '').toLowerCase();
      if (filterValue && filterValue.length > 0) {
        filteredAirports = airports.filter(
          a =>
            a.city.toLowerCase().includes(filterValue) ||
            a.iata.toLowerCase().includes(filterValue)
        );
      }
      return filteredAirports;
    } else if (airports) {
      filteredAirports = airports;
    } else {
      filteredAirports = [];
    }
    return filteredAirports.length > 10
      ? filteredAirports.slice(0, 10)
      : filteredAirports;
  }

  setJourneyType(journeyType: TBOFlightJourneyType) {
    this.selectedJourneyType = journeyType;
    this.flightSearchService.setJourneyType(this.flightSearchForm, journeyType);
  }

  mapToAirportName(airportCode?: string | Airport): string {
    if (this._airports && this._airports.length) {
      if (airportCode && typeof airportCode === typeof '') {
        const airport = this._airports.find(a => a.iata === airportCode);
        return airportCode ? `${airport.city} ${airport.iata}` : '';
      } else if (airportCode) {
        const airport = airportCode as Airport;
        return `${airport.city} ${airport.iata}`;
      }
    } else {
      return '';
    }
  }

  ngOnInit() {}

  onFiltersChange(filters: TBOFlightsFilter[]) {
    this.filters$.next(filters);
  }
  openBottomSheet(): void {
    this._bottomSheet.open(OneWayFlightsSearchMobileFilter);
  }

  loadFlights() {
    this.loadFlights$.next(true);
  }
}
