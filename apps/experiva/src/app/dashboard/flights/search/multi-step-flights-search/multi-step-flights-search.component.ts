import { Component, OnInit, EventEmitter, Output, Input, ElementRef, ViewChild, SimpleChanges, OnChanges } from '@angular/core';
import { TBOFlightsFilter, TBOFlight, Airline } from '@experiva/api-interfaces';
import * as moment from 'moment';
import { last } from 'rxjs/operators';
@Component({
  selector: 'experiva-multi-step-flights-search',
  templateUrl: './multi-step-flights-search.component.html',
  styleUrls: ['./multi-step-flights-search.component.scss']
})
export class MultiStepFlightsSearchComponent implements OnInit, OnChanges {

  airlinesAvailable: Map<string, Airline> = new Map<string, Airline>();
  trips: {
    source: string;
    destination: string;
  }[] = [];
  moment = moment;
  _filters: {
    departureTime: ('Morning' | 'Afternoon' | 'Evening' | 'Night')[];
    refundable: (true | false)[];
    stops: (0 | 1 | 2)[];
    airlines: string[];
  } = {
      departureTime: [],
      refundable: [],
      stops: [],
      airlines: []
    };
  @Output() filters: EventEmitter<TBOFlightsFilter[]> = new EventEmitter();

  @Input() loading: boolean;

  @Input() flights: TBOFlight[][];
  @Input() availableAirlines: Airline[][];

  @ViewChild('filtersEl', { static: true }) filtersEl: ElementRef<
    HTMLDivElement
  >;

  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
    if (
      changes['availableAirlines'] &&
      changes['availableAirlines'].currentValue
    ) {
      const airlines = changes['availableAirlines'].currentValue[0] as Airline[];
      this.airlinesAvailable = airlines.reduce((acc, airline) => {
        acc.set(airline.AirlineCode, airline);
        return acc;
      }, new Map<string, Airline>());
    }

    if (changes['flights'] && changes['flights'].currentValue) {
      const flights = changes['flights'].currentValue as TBOFlight[][];
      const trips = [];
      if (flights.length && flights[0].length) {
        flights[0][0].Segments.forEach(s => {
          const totalFlights = s.length;
          const firstFlight = s[0];
          const lastFlight = s[totalFlights - 1];
          trips.push({
            source: firstFlight.Origin.Airport.CityName,
            destination: lastFlight.Destination.Airport.CityName
          });
        })
      }
      this.trips = trips;
    }
  }

  ngOnInit() {
    this.filters.emit([this._filters]);
  }

  filterDepartureTime(
    time: 'Morning' | 'Afternoon' | 'Evening' | 'Night',
    checked: boolean
  ) {
    const timeIndex = this._filters.departureTime.indexOf(time);
    if (timeIndex >= 0) {
      if (!checked) {
        this._filters.departureTime.splice(timeIndex, 1);
      }
    } else {
      if (checked) {
        this._filters.departureTime.push(time);
      }
    }
    this.filters.emit([this._filters]);
  }

  filterRefundableFlights(isRefundable: boolean, checked: boolean) {
    const index = isRefundable
      ? this._filters.refundable.indexOf(true)
      : this._filters.refundable.indexOf(false);
    if (checked && index === -1) {
      this._filters.refundable.push(isRefundable);
    } else if (!checked && index >= 0) {
      this._filters.refundable.splice(index, 1);
    }
    this.filters.emit([this._filters]);
  }

  filterFilghtsByStops(stopCount: 0 | 1 | 2, checked: boolean) {
    const index = this._filters.stops.indexOf(stopCount);
    if (checked && index === -1) {
      this._filters.stops.push(stopCount);
    } else if (!checked && index >= 0) {
      this._filters.stops.splice(index, 1);
    }
    this.filters.emit([this._filters]);
  }
  filterFlightsByAirline(airlineCode: string, checked: boolean) {
    const index = this._filters.airlines.indexOf(airlineCode);
    if (checked && index === -1) {
      this._filters.airlines.push(airlineCode);
    } else if (!checked && index >= 0) {
      this._filters.airlines.splice(index, 1);
    }
    this.filters.emit([this._filters]);
  }

}
