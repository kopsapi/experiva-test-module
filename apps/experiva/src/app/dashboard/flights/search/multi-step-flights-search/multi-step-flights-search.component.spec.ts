import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiStepFlightsSearchComponent } from './multi-step-flights-search.component';

describe('MultiStepFlightsSearchComponent', () => {
  let component: MultiStepFlightsSearchComponent;
  let fixture: ComponentFixture<MultiStepFlightsSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultiStepFlightsSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiStepFlightsSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
