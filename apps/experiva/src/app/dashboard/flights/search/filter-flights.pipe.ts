import { Pipe, PipeTransform } from '@angular/core';
import {
  TBOFlight,
  Destination,
  Origin,
  TBOFlightsFilter
} from '@experiva/api-interfaces';
import * as moment from 'moment';
import { getTimeDiff } from '../../../shared/helpers/time-helpers';
@Pipe({
  name: 'filterFlights'
})
export class FilterFlights implements PipeTransform {
  transform(trips: TBOFlight[], filters: TBOFlightsFilter) {
    if (trips && filters) {
      const filghtsFilteredByDepartureTime = [];
      let filghtsFilteredByRefundableStatus = [];
      const filghtsFilteredByStops = [];
      let filghtsFilteredByAirline = [];

      // Filter by departure time
      if (filters.departureTime.length > 0) {
        trips.forEach(f => {
          for (let index = 0; index < filters.departureTime.length; index++) {
            const time = filters.departureTime[index];
            const departureMoment = moment(
              f.Segments[0][0].Origin.DepTime
            ).format('HH:mm');
            const departureTime = +departureMoment
              .split(':')
              .map((val, i) => (i ? `${+val / 60}` : val))
              .reduce((acc, curr) => acc + +curr, 0);

            switch (time) {
              case 'Morning':
                departureTime >= 0 && departureTime < 12
                  ? filghtsFilteredByDepartureTime.push(f)
                  : 0;
                break;
              case 'Afternoon':
                departureTime >= 12 && departureTime < 16
                  ? filghtsFilteredByDepartureTime.push(f)
                  : 0;
                break;
              case 'Evening':
                departureTime >= 16 && departureTime < 21
                  ? filghtsFilteredByDepartureTime.push(f)
                  : 0;
                break;
              case 'Night':
                departureTime >= 21 && departureTime <= 24
                  ? filghtsFilteredByDepartureTime.push(f)
                  : 0;
                break;
            }
          }
        });
      }

      // Filter by refundable status
      if (filters.refundable.length) {
        filghtsFilteredByRefundableStatus = trips.filter(
          f => filters.refundable.indexOf(f.IsRefundable) >= 0
        );
      }

      // Filter by number of stops
      if (filters.stops.length) {
        trips.forEach(f => {
          filters.stops.forEach(stopCount => {
            switch (stopCount) {
              case 0:
              case 1:
                f.Segments[0].length === stopCount + 1
                  ? filghtsFilteredByStops.push(f)
                  : 0;
                break;
              default:
                f.Segments[0].length > 2 ? filghtsFilteredByStops.push(f) : 0;
                break;
            }
          });
        });
      }

      // Filter by Airline
      if (filters.airlines.length) {
        filghtsFilteredByAirline = trips.filter(
          f => filters.airlines.indexOf(f.AirlineCode) >= 0
        );
      }

      const _filteredFlightDetails = this.intersect(
        this.intersect(
          this.intersect(
            filters.departureTime.length
              ? filghtsFilteredByDepartureTime
              : null,
            filters.refundable.length ? filghtsFilteredByRefundableStatus : null
          ),
          filters.stops.length ? filghtsFilteredByStops : null
        ),
        filters.airlines.length ? filghtsFilteredByAirline : null
      );
      const filteredFlightDetails =
        !filters.departureTime.length &&
        !filters.refundable.length &&
        !filters.stops.length &&
        !filters.airlines.length
          ? trips
          : _filteredFlightDetails;
      filteredFlightDetails.forEach(filghtOption => {
        const oneWayTripSegments = filghtOption.Segments[0];
        filghtOption.TimeToTravel = this.getTimDiff(
          (oneWayTripSegments[0].Origin as Origin).DepTime,
          (oneWayTripSegments[oneWayTripSegments.length - 1]
            .Destination as Destination).ArrTime
        );
      });

      return filteredFlightDetails;
    } else {
      return trips;
    }
  }

  getTimDiff(
    start: moment.Moment | number | string,
    end: moment.Moment | number | string
  ) {
    getTimeDiff(start, end);
  }

  intersect(set1: TBOFlight[] = null, set2: TBOFlight[] = null) {
    if (set1 === null && set2 === null) {
      return null;
    } else if (set1 === null) {
      return set2;
    } else if (set2 === null) {
      return set1;
    } else {
      return set1.reduce((intersection, el) => {
        const existsInBoth = set2.find(it => it.ResultIndex === el.ResultIndex);
        if (existsInBoth) {
          intersection.push(el);
        }
        return intersection;
      }, []);
    }
  }
}
