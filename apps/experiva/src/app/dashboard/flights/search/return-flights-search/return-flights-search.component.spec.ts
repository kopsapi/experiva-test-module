import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReturnFlightsSearchComponent } from './return-flights-search.component';

describe('ReturnFlightsSearchComponent', () => {
  let component: ReturnFlightsSearchComponent;
  let fixture: ComponentFixture<ReturnFlightsSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReturnFlightsSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReturnFlightsSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
