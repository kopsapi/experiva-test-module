import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  Input,
  ChangeDetectionStrategy,
  OnChanges,
  SimpleChanges,
  ElementRef,
  ViewChild,
  Inject
} from '@angular/core';
import {
  TBOFlight,
  Airline,
  TBOFlightsFilter,
  TBOFlightSegment
} from '@experiva/api-interfaces';
import * as moment from 'moment';
import { SESSION_STORAGE } from 'apps/experiva/src/app/core/services/storage/session-storage.token';
import { StorageService } from 'apps/experiva/src/app/core/services/storage/storage.service';
import { SessionStorageKeys } from 'apps/experiva/src/app/shared/constants/session-storage-keys';
import { Router } from '@angular/router';

@Component({
  selector: 'experiva-return-flights-search',
  templateUrl: './return-flights-search.component.html',
  styleUrls: ['./return-flights-search.component.scss']
})
export class ReturnFlightsSearchComponent implements OnInit, OnChanges {
  airlinesAvailable: Map<string, Airline> = new Map<string, Airline>();
  moment = moment;
  _filters: TBOFlightsFilter[] = [
    {
      departureTime: [],
      refundable: [],
      stops: [],
      airlines: []
    },
    {
      departureTime: [],
      refundable: [],
      stops: [],
      airlines: []
    }
  ];
  @Output() filters: EventEmitter<TBOFlightsFilter[]> = new EventEmitter();

  @Input() loading: boolean;

  @Input() flights: TBOFlight[][];
  @Input() availableAirlines: Airline[][];
  onwardsFlight: TBOFlight;
  returnFlight: TBOFlight;
  @ViewChild('filtersEl', { static: true }) filtersEl: ElementRef<
    HTMLDivElement
  >;

  constructor(
    @Inject(SESSION_STORAGE) private sessionStorage: StorageService,
    private readonly router: Router
  ) {
    this.sessionStorage.delete(SessionStorageKeys.selectedFlights);
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (
      changes['availableAirlines'] &&
      changes['availableAirlines'].currentValue
    ) {
      const airlines = changes['availableAirlines']
        .currentValue[0] as Airline[];
      this.airlinesAvailable = airlines.reduce((acc, airline) => {
        acc.set(airline.AirlineCode, airline);
        return acc;
      }, new Map<string, Airline>());
    }
  }

  ngOnInit() {
    this.filters.emit(this._filters);
  }

  filterDepartureTime(
    time: 'Morning' | 'Afternoon' | 'Evening' | 'Night',
    checked: boolean
  ) {
    const timeIndex = this._filters[0].departureTime.indexOf(time);
    if (timeIndex >= 0) {
      if (!checked) {
        this._filters[0].departureTime.splice(timeIndex, 1);
      }
    } else {
      if (checked) {
        this._filters[0].departureTime.push(time);
      }
    }
    this.filters.emit(this._filters);
  }
  filterReturnDepartureTime(
    time: 'Morning' | 'Afternoon' | 'Evening' | 'Night',
    checked: boolean
  ) {
    const timeIndex = this._filters[1].departureTime.indexOf(time);
    if (timeIndex >= 0) {
      if (!checked) {
        this._filters[1].departureTime.splice(timeIndex, 1);
      }
    } else {
      if (checked) {
        this._filters[1].departureTime.push(time);
      }
    }
    this.filters.emit(this._filters);
  }

  filterRefundableFlights(isRefundable: boolean, checked: boolean) {
    const index = isRefundable
      ? this._filters[0].refundable.indexOf(true)
      : this._filters[0].refundable.indexOf(false);
    if (checked && index === -1) {
      this._filters[0].refundable.push(isRefundable);
      this._filters[1].refundable.push(isRefundable);
    } else if (!checked && index >= 0) {
      this._filters[0].refundable.splice(index, 1);
      this._filters[1].refundable.splice(index, 1);
    }
    this.filters.emit(this._filters);
  }

  filterFilghtsByStops(stopCount: 0 | 1 | 2, checked: boolean) {
    // const index = this._filters.stops.indexOf(stopCount);
    // if (checked && index === -1) {
    //   this._filters.stops.push(stopCount);
    // } else if (!checked && index >= 0) {
    //   this._filters.stops.splice(index, 1);
    // }
    // this.filters.emit(this._filters);
  }
  filterFlightsByAirline(airlineCode: string, checked: boolean) {
    // const index = this._filters.airlines.indexOf(airlineCode);
    // if (checked && index === -1) {
    //   this._filters.airlines.push(airlineCode);
    // } else if (!checked && index >= 0) {
    //   this._filters.airlines.splice(index, 1);
    // }
    // this.filters.emit(this._filters);
  }

  openFilters() {
    this.filtersEl.nativeElement.classList.add('visible');
  }

  review() {
    this.sessionStorage.set(SessionStorageKeys.selectedFlights, [
      this.onwardsFlight,
      this.returnFlight
    ]);
    this.router.navigate(['/dashboard/flights/review/return']);
  }
}
