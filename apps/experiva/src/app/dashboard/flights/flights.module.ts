import { NgModule } from '@angular/core';
import { FlightsComponent } from './flights.component';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { StorageModule } from '../../core/services/storage/storage.module';

@NgModule({
  declarations: [FlightsComponent],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: FlightsComponent,
        children: [
          {
            path: 'search',
            loadChildren: () =>
              import('./search/search.module').then(m => m.SearchModule)
          },
          {
            path: 'review',
            loadChildren: () =>
              import('./flight-review/flight-review.module').then(
                m => m.FlightReviewModule
              )
          }
          // {
          //   path: 'return',
          //   loadChildren: () => import('./return-flights/return-flights.module').then(m => m.ReturnFlightsModule)
          // }
        ]
      }
    ])
  ],
  providers: []
})
export class FlightsModule {}
