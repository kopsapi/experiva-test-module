import { Component, Inject } from '@angular/core';
import { SESSION_STORAGE } from '../../../../core/services/storage/session-storage.token';
import { StorageService } from '../../../../core/services/storage/storage.service';
import { SessionStorageKeys } from '../../../../shared/constants/session-storage-keys';
import { TBOFlight, ITBOFlightExtraCharge } from '@experiva/api-interfaces';
import * as moment from 'moment';
import { getTimeDiff } from '../../../../shared/helpers/time-helpers';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'experiva-one-way-flight-review',
  templateUrl: 'one-way-flight-review.component.html',
  styleUrls: ['one-way-flight-review.component.scss']
})
export class OneWayFlightReviewComponent {
  selectedFlight: TBOFlight;
  totalTravelTimeInMinutes = 0;
  totalTravelTime: string;
  totalStops = 0;
  moment = moment;
  fare: {
    baseFare: number;
    otherCharges: number;
    taxes: number;
    gst: number;
  } = {} as any;
  constructor(
    @Inject(SESSION_STORAGE) private readonly storage: StorageService,
    private readonly route: ActivatedRoute
  ) {
    const selectedFlights: TBOFlight[] = this.storage.get(
      SessionStorageKeys.selectedFlights
    ) as TBOFlight[];
    const now = Date.now();
    // moment().format('moment');
    // this.selectedFlight.Segments[0][0].Origin.DepTime;
    if (selectedFlights && (selectedFlights as TBOFlight[]).length) {
      this.route.data.subscribe(
        ({ margin }: { margin: ITBOFlightExtraCharge }) => {
          let baseFare =
            selectedFlights[0].Fare.BaseFare * (1 + margin.baseMargin / 100);
          baseFare = Math.round(baseFare);
          this.fare.baseFare = baseFare;
          this.fare.otherCharges =
            selectedFlights[0].Fare.OtherCharges +
            selectedFlights[0].Fare.TdsOnCommission +
            selectedFlights[0].Fare.TdsOnIncentive +
            selectedFlights[0].Fare.TdsOnPLB +
            selectedFlights[0].Fare.ServiceFee;
          this.fare.taxes = selectedFlights[0].Fare.Tax;
          this.fare.gst = Math.round(this.fare.baseFare * (margin.gst / 100));
          this.selectedFlight = selectedFlights[0];
          this.selectedFlight.Segments.forEach(segment => {
            segment.forEach(trip => {
              this.totalTravelTimeInMinutes +=
                (+trip.GroundTime || 0) + (+trip.Duration || 0);
            });
          });
          this.totalTravelTime = getTimeDiff(
            now,
            now + this.totalTravelTimeInMinutes * 60 * 1000
          );
          this.totalStops = this.selectedFlight.Segments[0].length - 1;
          console.log(this.selectedFlight);
        }
      );
    }
  }

  getTimeDiff = (duration: number) => {
    const now = Date.now();
    return getTimeDiff(now, now + duration * 60 * 1000);
  };
}
