import { Component } from '@angular/core';

@Component({
  selector: 'experiva-flight-review',
  templateUrl: './flight-review.component.html',
  styleUrls: ['./flight-review.component.scss']
})
export class FlightReviewComponent {}
