import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';
import { MaterialModule } from '../../../material/material.module';
import { RouterModule, Routes } from '@angular/router';
import { FlightReviewComponent } from './flight-review.component';
import { OneWayFlightReviewComponent } from './one-way-flight-review/one-way-flight-review.component';
import { ReturnFlightReviewComponent } from './return-flight-review/return-flight-review.component';
import { FlightMarginResolver } from './flight-margin-resolver';

const routes: Routes = [
  {
    path: '',
    component: FlightReviewComponent,
    children: [
      {
        path: 'onwards',
        component: OneWayFlightReviewComponent,
        resolve: {
          margin: FlightMarginResolver
        }
      },
      {
        path: 'return',
        component: ReturnFlightReviewComponent,
        resolve: {
          margin: FlightMarginResolver
        }
      }
    ]
  }
];
@NgModule({
  imports: [SharedModule, MaterialModule, RouterModule.forChild(routes)],
  providers: [FlightMarginResolver],
  declarations: [
    FlightReviewComponent,
    OneWayFlightReviewComponent,
    ReturnFlightReviewComponent
  ]
})
export class FlightReviewModule {}
