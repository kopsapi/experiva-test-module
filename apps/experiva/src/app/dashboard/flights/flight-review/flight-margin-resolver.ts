import { Injectable } from '@angular/core';
import { FlightReviewModule } from './flight-review.module';
import { ApiService } from '../../../core/services/http/api.service';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { ITBOFlightExtraCharge } from '@experiva/api-interfaces';

@Injectable()
export class FlightMarginResolver implements Resolve<ITBOFlightExtraCharge> {
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<ITBOFlightExtraCharge> {
    return this.getMargin();
  }
  constructor(private readonly api: ApiService) {}
  getMargin() {
    return this.api.sendRequest({
      method: 'get',
      endpoint: 'tbo-flights/margin'
    });
  }
}
