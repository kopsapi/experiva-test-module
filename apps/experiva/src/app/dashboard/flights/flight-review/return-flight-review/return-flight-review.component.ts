import { Component, Inject } from '@angular/core';
import { TBOFlight, ITBOFlightExtraCharge } from '@experiva/api-interfaces';
import * as moment from 'moment';
import { SESSION_STORAGE } from '../../../../core/services/storage/session-storage.token';
import { StorageService } from '../../../../core/services/storage/storage.service';
import { SessionStorageKeys } from '../../../../shared/constants/session-storage-keys';
import { getTimeDiff } from '../../../../shared/helpers/time-helpers';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'experiva-return-flight-review',
  templateUrl: 'return-flight-review.component.html',
  styleUrls: ['return-flight-review.component.scss']
})
export class ReturnFlightReviewComponent {
  onwardFlight: TBOFlight;
  returnFlight: TBOFlight;
  totalOnwardTravelTime = '';
  totalReturnTravelTime = '';
  totalOnwardStops = 0;
  totalReturnStops = 0;
  moment = moment;
  fare: {
    baseFare: number;
    otherCharges: number;
    taxes: number;
    gst: number;
  } = {} as any;
  constructor(
    @Inject(SESSION_STORAGE) private readonly storage: StorageService,
    private readonly route: ActivatedRoute
  ) {
    const selectedFlights = this.storage.get(
      SessionStorageKeys.selectedFlights
    );
    console.log(selectedFlights);
    if (selectedFlights && (selectedFlights as TBOFlight[]).length) {
      this.route.data.subscribe(
        ({ margin }: { margin: ITBOFlightExtraCharge }) => {
          [
            this.onwardFlight,
            this.returnFlight
          ] = selectedFlights as TBOFlight[];
          [
            this.totalOnwardTravelTime,
            this.totalOnwardStops
          ] = this.getTripTime(this.onwardFlight);
          [
            this.totalReturnTravelTime,
            this.totalReturnStops
          ] = this.getTripTime(this.returnFlight);
          let baseFare =
            (this.onwardFlight.Fare.BaseFare +
              this.returnFlight.Fare.BaseFare) *
            (1 + margin.baseMargin / 100);
          baseFare = Math.round(baseFare);
          this.fare.baseFare = baseFare;
          this.fare.otherCharges =
            this.onwardFlight.Fare.OtherCharges +
            this.returnFlight.Fare.OtherCharges +
            (this.onwardFlight.Fare.TdsOnCommission +
              this.returnFlight.Fare.TdsOnCommission) +
            (this.onwardFlight.Fare.TdsOnIncentive +
              this.returnFlight.Fare.TdsOnIncentive) +
            (this.onwardFlight.Fare.TdsOnPLB +
              this.returnFlight.Fare.TdsOnPLB) +
            (this.onwardFlight.Fare.ServiceFee +
              this.returnFlight.Fare.ServiceFee);
          this.fare.taxes =
            this.onwardFlight.Fare.Tax + this.returnFlight.Fare.Tax;
          this.fare.gst = Math.round(this.fare.baseFare * (margin.gst / 100));
        }
      );
    }
  }
  getTimeDiff(duration: number): string;
  getTimeDiff(from: any, to: any): string;
  getTimeDiff(durationOrFrom: any, to?: any) {
    if (!to) {
      const now = Date.now();
      return getTimeDiff(now, now + durationOrFrom * 60 * 1000);
    } else {
      return getTimeDiff(durationOrFrom, to);
    }
  }

  private getTripTime(flight: TBOFlight): [string, number] {
    const now = Date.now();
    let totalTimeInMinutes = 0;
    flight.Segments.forEach(segment => {
      segment.forEach(trip => {
        totalTimeInMinutes += (+trip.GroundTime || 0) + (+trip.Duration || 0);
      });
    });
    return [
      getTimeDiff(now, now + totalTimeInMinutes * 60 * 1000),
      flight.Segments[0].length - 1
    ];
  }
}
