import { Airport } from '@experiva/api-interfaces';
import { EntityState, createEntityAdapter } from "@ngrx/entity";
import { createReducer, on } from '@ngrx/store';
import { airportsLoaded } from '../dashboard.actions';
export interface DashboardState {
    // additional entity state properties
    airports: Airport[]
}

export const initialDashboardState = {
    airports: []
};

export const dashboardReducersMap = createReducer(initialDashboardState,
    on(airportsLoaded, (state, action) => {
        return { airports: action.airports };
    }))