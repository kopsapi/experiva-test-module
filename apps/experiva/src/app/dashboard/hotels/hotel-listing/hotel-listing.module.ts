import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HotelListingComponent } from './hotel-listing.component';
import { SharedModule } from '../../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../../../material/material.module';



@NgModule({
  declarations: [HotelListingComponent],
  imports: [
    SharedModule,
    MaterialModule,
    RouterModule.forChild([{
      path: '',
      component: HotelListingComponent
    }])
  ]
})
export class HotelListingModule { }
