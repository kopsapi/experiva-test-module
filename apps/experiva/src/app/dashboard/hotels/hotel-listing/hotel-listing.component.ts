import { Component, OnInit, Inject } from '@angular/core';
import { SESSION_STORAGE } from '../../../core/services/storage/session-storage.token';
import { StorageService } from '../../../core/services/storage/storage.service';
import { TBOHotelSearchRequest } from '@experiva/api-interfaces';
import { SessionStorageKeys } from '../../../shared/constants/session-storage-keys';
import { HotelSearchService } from '../../hotels-search.service';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'experiva-hotel-listing',
  templateUrl: './hotel-listing.component.html',
  styleUrls: ['./hotel-listing.component.scss']
})
export class HotelListingComponent implements OnInit {
  ngOnInit() {}
}
