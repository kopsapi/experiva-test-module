import { NgModule } from '@angular/core';
import { HotelsComponent } from './hotels.component';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [HotelsComponent],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: HotelsComponent,
        children: [
          {
            path: '',
            loadChildren: () => import('./hotel-listing/hotel-listing.module').then(m => m.HotelListingModule)
          },
          {
            path: ':id',
            loadChildren: () => import('./hotel-detail/hotel-detail.module').then(m => m.HotelDetailModule)
          }
        ]
      }
    ])
  ]
})
export class HotelsModule { }
