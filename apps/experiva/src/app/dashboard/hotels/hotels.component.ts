import { Component, OnInit, Inject } from '@angular/core';
import { HotelCity, cities } from '../../shared/constants/cities';
import { of } from 'rxjs';
import { HotelSearchService } from '../hotels-search.service';
import { FormControl, FormGroup } from '@angular/forms';
import 'hammerjs';
import { StorageService } from '../../core/services/storage/storage.service';
import { SESSION_STORAGE } from '../../core/services/storage/session-storage.token';
import { SessionStorageKeys } from '../../shared/constants/session-storage-keys';
import { TBOHotelSearchRequest } from '@experiva/api-interfaces';
@Component({
  selector: 'experiva-hotels',
  templateUrl: './hotels.component.html',
  styleUrls: ['./hotels.component.scss']
})
export class HotelsComponent implements OnInit {
  cities$ = of(cities);
  hotelSearchForm: FormGroup;
  destination: FormControl = new FormControl();
  constructor(
    @Inject(SESSION_STORAGE) private readonly sessionStorage: StorageService,
    private readonly hotelSearchService: HotelSearchService
  ) {
    this.hotelSearchForm = this.hotelSearchService.initForm(
      <TBOHotelSearchRequest>(
        this.sessionStorage.get(SessionStorageKeys.QueryForHotel)
      ) || null
    );
  }

  ngOnInit() {}

  mapToHotelCitytName(city?: HotelCity): string {
    return city ? `${city.CityName}` : undefined;
  }

  filterHotels(_cities: HotelCity[], searchString: string): HotelCity[] {
    let filteredCities = [];
    if (_cities && searchString && typeof searchString === typeof '') {
      const filterValue = (searchString || '').toLowerCase();
      if (filterValue && filterValue.length > 0) {
        filteredCities = _cities.filter(
          city =>
            city.CityName.toLowerCase().includes(filterValue) ||
            (city.StateProvince || '').toLowerCase().includes(filterValue)
        );
      }
      return filteredCities;
    } else if (_cities) {
      filteredCities = _cities;
    } else {
      filteredCities = [];
    }
    return filteredCities.length > 10
      ? filteredCities.slice(0, 10)
      : filteredCities;
  }
}
