import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../reducers';
import { loadAllAirports } from './dashboard.actions';

@Component({
  selector: 'experiva-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  constructor(private readonly store$: Store<AppState>) { }
  ngOnInit() {
    this.store$.dispatch(loadAllAirports());
  }
}
