import { createAction, props } from "@ngrx/store";
import { Airport } from '@experiva/api-interfaces';

export const loadAllAirports = createAction('[Dashboard Screen Load] Load All cities')
export const airportsLoaded = createAction('[Dashboard Effect] Airports loaded', props<{ airports: Airport[] }>())