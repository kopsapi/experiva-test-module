import { NgModule } from '@angular/core';
import { TestComponent } from './test.component';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FilterModule } from '../../shared/pipes/filter/filter.module';
// import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [TestComponent],
  imports: [
    SharedModule,
    FilterModule,
    RouterModule.forChild([
      {
        path: '',
        component: TestComponent,
        children: []
      }
    ])
  ]
})
export class TestModule { }
