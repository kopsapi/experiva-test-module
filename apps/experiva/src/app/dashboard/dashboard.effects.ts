import { Injectable } from "@angular/core";
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { loadAllAirports, airportsLoaded } from './dashboard.actions';
import { tap, concatMap, map, switchMap } from 'rxjs/operators';
import { DashboardService } from './dashboard.service';
import { of } from 'rxjs';

@Injectable()
export class DashboardEffects {
    loadAllAirports = createEffect(
        () => this.actions$.pipe(
            ofType(loadAllAirports),
            switchMap(() => this.dashboardService.loadAllAirports()),
            map((airports) => airportsLoaded({ airports }))
        )
    );

    constructor(private readonly actions$: Actions,
        private readonly dashboardService: DashboardService) { }
}