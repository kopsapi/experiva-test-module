import { NgModule } from '@angular/core';
import { DashboardComponent } from './dashboard.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { EffectsModule } from '@ngrx/effects';
import { DashboardEffects } from './dashboard.effects';
import { DashboardService } from './dashboard.service';
import { StoreModule } from '@ngrx/store';
import { dashboardReducersMap } from './reducers/dashboard.reducers';
import { FlightSearchService } from './flights-search.service';
import { HotelSearchService } from './hotels-search.service';
import { TestComponent } from './test/test.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: 'home',
        loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
      },
      {
        path: 'flights',
        loadChildren: () =>
          import('./flights/flights.module').then(m => m.FlightsModule)
      },
      {
        path: 'hotels',
        loadChildren: () =>
          import('./hotels/hotels.module').then(m => m.HotelsModule)
      },
      {
        path: 'test',
        loadChildren: () =>
          import('./test/test.module').then(m => m.TestModule)
      },
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'home'
      }
    ]
  }
];

@NgModule({
  declarations: [DashboardComponent, TestComponent],
  providers: [DashboardService, FlightSearchService, HotelSearchService],
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
    StoreModule.forFeature('dashboard', dashboardReducersMap),
    EffectsModule.forFeature([DashboardEffects])
  ]
})
export class DashboardModule {}
