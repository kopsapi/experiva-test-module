import { Component, OnInit, Inject } from '@angular/core';
import { AuthState } from '../../login/reducers';
import { Store } from '@ngrx/store';
import { logout } from '../../login/login.actions';
import { TBOFlightJourneyType } from '@experiva/enums';
import { ariportsSelector } from '../dashboard.selectors';
import { Airport, RoomGuest } from '@experiva/api-interfaces';
import { Router } from '@angular/router';
import { FlightSearchService } from '../flights-search.service';
import { of } from 'rxjs';
import { cities, HotelCity } from '../../shared/constants/cities';
import { HotelSearchService } from '../hotels-search.service';
import { FormControl, FormArray, FormGroup } from '@angular/forms';
import * as moment from 'moment';
import { StorageService } from '../../core/services/storage/storage.service';
import { SESSION_STORAGE } from '../../core/services/storage/session-storage.token';
import { SessionStorageKeys } from '../../shared/constants/session-storage-keys';
// import 'hammerjs';
@Component({
  selector: 'experiva-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  today = new Date();
  TBOFlightJourneyType = TBOFlightJourneyType;
  flightSearchForm = this.flightSearchService.getFreshForm();
  hotelSearchForm = this.hotelSearchService.initForm();
  hotelDestination = new FormControl('');
  hotelAdultsInRoom = new FormControl(1);
  airports$ = this.store.select(ariportsSelector);
  cities$ = of(cities);

  filterAirports(airports: Airport[], searchString: string): Airport[] {
    let filteredAirports = [];
    if (airports && searchString && typeof searchString === typeof '') {
      const filterValue = (searchString || '').toLowerCase();
      if (filterValue && filterValue.length > 0) {
        filteredAirports = airports.filter(
          a =>
            a.city.toLowerCase().includes(filterValue) ||
            a.iata.toLowerCase().includes(filterValue)
        );
      }
      return filteredAirports;
    } else if (airports) {
      filteredAirports = airports;
    } else {
      filteredAirports = [];
    }
    return filteredAirports.length > 10
      ? filteredAirports.slice(0, 10)
      : filteredAirports;
  }
  filterHotels(_cities: HotelCity[], searchString: string): HotelCity[] {
    let filteredCities = [];
    if (_cities && searchString && typeof searchString === typeof '') {
      const filterValue = (searchString || '').toLowerCase();
      if (filterValue && filterValue.length > 0) {
        filteredCities = _cities.filter(
          city =>
            city.CityName.toLowerCase().includes(filterValue) ||
            (city.StateProvince || '').toLowerCase().includes(filterValue)
        );
      }
      return filteredCities;
    } else if (_cities) {
      filteredCities = _cities;
    } else {
      filteredCities = [];
    }
    return filteredCities.length > 10
      ? filteredCities.slice(0, 10)
      : filteredCities;
  }

  constructor(
    private readonly store: Store<AuthState>,
    private readonly router: Router,
    private readonly flightSearchService: FlightSearchService,
    private readonly hotelSearchService: HotelSearchService,
    @Inject(SESSION_STORAGE) private readonly sessionStorage: StorageService
  ) {}

  mapToAirportName(airport?: Airport): string {
    console.log(airport);
    return airport ? `${airport.city} ${airport.iata}` : undefined;
  }
  mapToHotelCitytName(city?: HotelCity): string {
    return city ? `${city.CityName}` : undefined;
  }
  ngOnInit() {
    this.hotelDestination.valueChanges.subscribe((val: HotelCity) => {
      this.hotelSearchForm.get('CountryCode').setValue(val.CountryCode);
      this.hotelSearchForm.get('CityId').setValue(val.CityId);
    });
    const updateRoomGuest = (rooms: number) => {
      (this.hotelSearchForm.get('RoomGuests') as FormArray).clear();
      '.'
        .repeat(rooms)
        .split('')
        .forEach(() =>
          (this.hotelSearchForm.get('RoomGuests') as FormArray).push(
            this.hotelSearchService.roomFormGroup({
              NoOfAdults: this.hotelAdultsInRoom.value,
              NoOfChild: 0,
              ChildAge: []
            })
          )
        );
    };
    this.hotelSearchForm
      .get('NoOfRooms')
      .valueChanges.subscribe((rooms: number) => {
        updateRoomGuest(+rooms);
      });
    this.hotelAdultsInRoom.valueChanges.subscribe(() => {
      updateRoomGuest(+this.hotelSearchForm.get('NoOfRooms').value);
    });
  }

  updateNumberOfNights(checkoutDate: moment.Moment) {
    this.hotelSearchForm
      .get('NoOfNights')
      .setValue(
        moment(this.hotelSearchForm.get('CheckInDate').value).diff(
          checkoutDate,
          'days'
        )
      );
  }
  setJourneyType(selectedJourneyType: TBOFlightJourneyType) {
    this.flightSearchService.setJourneyType(
      this.flightSearchForm,
      selectedJourneyType
    );
  }

  addQuerySegment() {
    this.flightSearchService.addQuerySegment(this.flightSearchForm);
  }

  searchFlights() {
    const flightSearchForm = this.flightSearchService.getFlightSearchFormValue(
      this.flightSearchForm
    );
    if (flightSearchForm) {
      const segments = JSON.stringify(flightSearchForm.Segments);
      this.router.navigate(['dashboard', 'flights', 'search'], {
        queryParams: { ...flightSearchForm, Segments: segments }
      });
    } else {
    }
  }

  searchHotels() {
    this.sessionStorage.set(
      SessionStorageKeys.QueryForHotel,
      this.hotelSearchForm.value
    );
    this.router.navigate(['dashboard', 'hotels']);
  }

  logout() {
    this.store.dispatch(logout());
  }
}
