import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FilterModule } from '../../shared/pipes/filter/filter.module';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  }
]

@NgModule({
  declarations: [HomeComponent],
  imports: [
    SharedModule,
    FilterModule,
    RouterModule.forChild(routes)
  ]
})
export class HomeModule { }
