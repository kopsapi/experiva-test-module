import { Injectable } from "@angular/core";
import { ApiService } from '../core/services/http/api.service';
import { Observable, of } from 'rxjs';
import { Airport } from '@experiva/api-interfaces';
import { FormBuilder } from '@angular/forms';

@Injectable()
export class DashboardService {
    constructor(private readonly api: ApiService) { }

    loadAllAirports(): Observable<Airport[]> {
        return this.api.sendRequest({
            method: 'get',
            endpoint: 'airports.json',
            apiBase: './assets/resources/'
        });

    }


}