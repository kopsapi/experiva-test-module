import { createSelector, createFeatureSelector } from "@ngrx/store";
import { DashboardState } from './reducers/dashboard.reducers';

export const dashboardSelector = createFeatureSelector<DashboardState>('dashboard');

export const ariportsSelector = createSelector(dashboardSelector, (state) => state.airports);