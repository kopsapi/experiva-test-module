import { Injectable, Inject } from '@angular/core';
import {
  FormGroup,
  Validators,
  AbstractControl,
  FormBuilder,
  FormArray,
  FormControl
} from '@angular/forms';
import { TBOFlightJourneyType, TBOFlightCabinClass } from '@experiva/enums';
import { Moment } from 'moment';
import {
  TBOFlightQuerySegment,
  TBOFligthsQuery,
  Fare,
  FareBreakdown,
  TBOFlightSegment,
  FareRule,
  TBOFlight
} from '@experiva/api-interfaces';
import * as moment from 'moment';
import { Observable, of } from 'rxjs';
import { ApiService } from '../core/services/http/api.service';
import { map, filter, tap } from 'rxjs/operators';
import { LOCAL_STORAGE } from '../core/services/storage/local-storage.token';
import { StorageService } from '../core/services/storage/storage.service';

type OneWayFlightSearchResults = Array<TBOFlight>;
interface ReturnFlightSearchResults {
  onwardTrips: Array<TBOFlight>;
  returnTrips: Array<TBOFlight>;
}
interface MultiStopFlightSearchResults {
  tours: {
    trips: Array<TBOFlight>;
  }[];
}
@Injectable()
export class FlightSearchService {
  constructor(
    private readonly fb: FormBuilder,
    private readonly api: ApiService,
    @Inject(LOCAL_STORAGE) private readonly storage: StorageService
  ) {}
  getFreshForm(query: TBOFligthsQuery = null) {
    const flightSearchForm: FormGroup = this.fb.group({
      AdultCount: [
        query && query.AdultCount ? query.AdultCount : 1,
        [Validators.required]
      ],
      ChildCount: [query && query.ChildCount ? query.ChildCount : 0],
      InfantCount: [query && query.InfantCount ? query.InfantCount : 0],
      JourneyType: [
        query && query.JourneyType
          ? query.JourneyType
          : TBOFlightJourneyType.OneWay,
        [
          Validators.required,
          (control: AbstractControl) => {
            switch (control.value) {
              case TBOFlightJourneyType.OneWay:
              case TBOFlightJourneyType.Return:
              case TBOFlightJourneyType.SpecialReturn:
              case TBOFlightJourneyType.AdvanceSearch:
              case TBOFlightJourneyType.MultiStop:
                return null;
              default:
                return { JourneyType: 'invalid' };
            }
          }
        ]
      ],
      Segments: this.fb.array(
        query && query.Segments
          ? query.Segments.map(s => this.querySegment(s))
          : [this.querySegment()],
        [Validators.required]
      )
    });
    return flightSearchForm;
  }

  setJourneyType(form: FormGroup, selectedJourneyType: TBOFlightJourneyType) {
    form.get('JourneyType').setValue(selectedJourneyType);
    const segments = form.get('Segments') as FormArray;
    switch (selectedJourneyType) {
      case TBOFlightJourneyType.OneWay:
        if (segments.length > 1) {
          const length = segments.length;
          for (let index = length - 1; index >= 1; index--) {
            segments.removeAt(index);
          }
        }
        break;
      case TBOFlightJourneyType.Return:
        // If Journey is a return then keep only one segment
        // set return date in PreferredArrivalTime
        // While getting the form value, we will add the actual segment
        if (segments.length > 1) {
          const length = segments.length;
          for (let index = 1; index < length; index++) {
            segments.removeAt(index);
          }
        }
        const onwardSegment = segments.at(0) as FormGroup;
        const returnSegment: FormGroup = this.querySegment(onwardSegment.value);
        returnSegment
          .get('Origin')
          .setValue(onwardSegment.get('Destination').value);
        returnSegment
          .get('Destination')
          .setValue(onwardSegment.get('Origin').value);
        segments.push(returnSegment);
        break;
    }
  }

  addQuerySegment(form: FormGroup) {
    const segments = form.get('Segments') as FormArray;
    segments.push(this.querySegment());
  }

  querySegment(segment: TBOFlightQuerySegment = null) {
    return this.fb.group({
      Origin: [
        segment && segment.Origin ? segment.Origin : '',
        [Validators.required]
      ],
      Destination: [
        segment && segment.Destination ? segment.Destination : '',
        [Validators.required]
      ],
      FlightCabinClass: [
        segment && segment.FlightCabinClass
          ? segment.FlightCabinClass
          : TBOFlightCabinClass.All,
        [Validators.required]
      ],
      PreferredDepartureTime: [
        segment && segment.PreferredDepartureTime
          ? moment(segment.PreferredDepartureTime)
          : undefined,
        [Validators.required]
      ],
      PreferredArrivalTime: [
        segment && segment.PreferredArrivalTime
          ? moment(segment.PreferredArrivalTime)
          : undefined
      ]
    });
  }
  getFlightSearchFormValue(form: FormGroup) {
    try {
      const query = {
        ...form.value,
        Segments: [...form.value.Segments]
      } as TBOFligthsQuery;
      if (query.JourneyType === TBOFlightJourneyType.Return) {
        query.Segments.splice(1);
        const sourceSegment = query.Segments[0];
        const returnDate = ((form.get('Segments') as FormArray)
          .at(0)
          .get('PreferredArrivalTime').value as Moment).toISOString();
        const returnSegment: TBOFlightQuerySegment = {
          Origin: sourceSegment.Destination,
          Destination: sourceSegment.Origin,
          FlightCabinClass: TBOFlightCabinClass.All,
          PreferredDepartureTime: returnDate,
          PreferredArrivalTime: returnDate
        };
        sourceSegment.PreferredArrivalTime =
          sourceSegment.PreferredDepartureTime;
        query.Segments.push(returnSegment);
      }
      query.Segments.forEach(s => {
        s.PreferredArrivalTime = s.PreferredDepartureTime =
          typeof s.PreferredDepartureTime === typeof ''
            ? s.PreferredDepartureTime
            : (<Moment>s.PreferredDepartureTime).toISOString();
        if (
          s.Origin &&
          typeof s.Origin !== typeof '' &&
          (s.Origin as Object).hasOwnProperty('iata')
        ) {
          s.Origin = (s.Origin as any).iata;
        }
        if (
          s.Destination &&
          typeof s.Destination !== typeof '' &&
          (s.Destination as Object).hasOwnProperty('iata')
        ) {
          s.Destination = (s.Destination as any).iata;
        }
      });
      return query;
    } catch (error) {
      return null;
    }
  }

  private endpoint(query: TBOFligthsQuery) {
    switch (query.JourneyType) {
      case TBOFlightJourneyType.OneWay:
        return 'one-way';
      case TBOFlightJourneyType.Return:
        return 'return';
      case TBOFlightJourneyType.MultiStop:
        return 'multi-stop';

      default:
        break;
    }
  }
  flights$(query: TBOFligthsQuery | FormGroup): Observable<TBOFlight[][]> {
    let _query: TBOFligthsQuery;
    if (query instanceof FormGroup) {
      _query = this.getFlightSearchFormValue(query);
    } else {
      _query = query;
    }
    if (_query) {
      // const tours = this.storage.get(_query.JourneyType) as TBOFlight[][];
      // if (tours) {
      //   return of(tours);
      // }
      return this.api
        .sendRequest({
          method: 'post',
          body: _query,
          endpoint: 'tbo-flights/' + this.endpoint(_query)
        })
        .pipe(
          filter(res => res.Response),
          map(res => res.Response.Results)
          // tap(res => {
          // this.storage.set(_query.JourneyType, res);
          // }),
        );
    }
    return of([]);
  }

  getOneWayTripResponse(flights: TBOFlight[]) {}
  availableAirlines() {}
}
