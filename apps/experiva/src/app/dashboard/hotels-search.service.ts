import { Injectable, Inject } from '@angular/core';
import {
  FormGroup,
  Validators,
  AbstractControl,
  FormBuilder,
  FormArray,
  FormControl,
  ValidationErrors
} from '@angular/forms';
import * as moment from 'moment';
import { Observable, of } from 'rxjs';
import { ApiService } from '../core/services/http/api.service';
import { map, filter, tap } from 'rxjs/operators';
import { LOCAL_STORAGE } from '../core/services/storage/local-storage.token';
import { StorageService } from '../core/services/storage/storage.service';
import { RoomGuest, TBOHotelSearchRequest } from '@experiva/api-interfaces';
@Injectable()
export class HotelSearchService {
  constructor(
    private readonly fb: FormBuilder,
    private readonly api: ApiService,
    @Inject(LOCAL_STORAGE) private readonly storage: StorageService
  ) {}

  initForm(query: TBOHotelSearchRequest = null) {
    return this.fb.group({
      CheckInDate: [
        query && query.CheckInDate ? query.CheckInDate : moment().add(1, 'day'),
        [
          Validators.required,
          (control: AbstractControl) => {
            if (control.value && moment(control.value).isBefore(moment())) {
              return {
                message: 'Checkin Date cannot be in the past'
              };
            } else {
              return null;
            }
          }
        ]
      ],
      NoOfNights: [
        query && query.NoOfNights ? query.NoOfNights : 0,
        [Validators.required, Validators.min(1)]
      ],
      CountryCode: [
        query && query.CountryCode ? query.CountryCode : 'IN',
        Validators.required
      ],
      CityId: [, Validators.required],
      PreferredCurrency: ['INR'],
      GuestNationality: ['IN'],
      NoOfRooms: [
        query && query.NoOfRooms ? query.NoOfRooms : 1,
        [Validators.required, Validators.min(1)]
      ],
      RoomGuests: this.fb.array(
        query && query.RoomGuests && query.RoomGuests.length
          ? query.RoomGuests.map(r => this.roomFormGroup(r))
          : [this.roomFormGroup()],
        [Validators.required, Validators.minLength(1)]
      ),
      MaxRating: [5],
      MinRating: [1]
    });
  }

  roomFormGroup(room: RoomGuest = null) {
    const roomGuestGroup = this.fb.group({
      NoOfAdults: [
        (room && room.NoOfAdults) || 1,
        [Validators.required, Validators.min(1)]
      ],
      NoOfChild: [0, [Validators.required, Validators.min(0)]],
      ChildAge: [[]]
    });
    roomGuestGroup.get('ChildAge').setValidators(this.validateChildrenAge);
    return roomGuestGroup;
  }

  validateChildrenAge(control: AbstractControl): ValidationErrors {
    const minLength = control.parent.get('NoOfChild').value || 0;
    let error: ValidationErrors;
    if (control.value) {
      if (typeof control.value === typeof []) {
        if (control.value.length !== minLength) {
          return {
            message: 'Child count and child age specification mismatch'
          };
        }
        for (let childIndex = 0; childIndex < minLength; childIndex++) {
          error =
            control.value[childIndex] < 0 || control.value[childIndex] > 18
              ? {
                  message: 'Child age should be between 0 and 18'
                }
              : null;
          if (error) {
            return error;
          }
        }
      } else {
        return {
          message: 'ChildAge should be an array'
        };
      }
      return error;
    } else {
      return null;
    }
  }
}
