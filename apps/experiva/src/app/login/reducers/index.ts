import { createReducer, on, ActionReducerMap } from '@ngrx/store';
import { User } from '../../shared/models/user';
import { login, logout } from '../login.actions';

export interface AuthState {
  user: User;
}
const initialAuthState: AuthState = {
  user: undefined
};
export const authReducers: ActionReducerMap<AuthState> = {
  user: createReducer(
    initialAuthState.user,
    on(login, (state, action) => {
      return action.user;
    }),
    on(logout, (state, action) => {
      return undefined;
    })
  )
};
