import { createAction, props } from '@ngrx/store';
import { User } from '../shared/models/user';

export const login = createAction(
  '[Login Screen] User Login',
  props<{ user: User }>()
);
export const logout = createAction('[Main Menu] User Logout');
