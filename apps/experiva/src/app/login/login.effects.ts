import { createEffect, Actions, ofType } from '@ngrx/effects';
import { Injectable, Inject } from '@angular/core';
import { login, logout } from './login.actions';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { StorageService } from '../core/services/storage/storage.service';
import { LocalStorageKeys } from '../shared/constants/storage-keys';
import { LOCAL_STORAGE } from '../core/services/storage/local-storage.token';
@Injectable()
export class LoginEffects {
  loginEffect = createEffect(
    () =>
      this.actions$.pipe(
        ofType(login),
        tap(action => {
          this.storageService.set(LocalStorageKeys.user, action.user);
          this.router.navigate(['/dashboard']);
        })
      ),
    {
      dispatch: false
    }
  );

  logoutEffect = createEffect(
    () =>
      this.actions$.pipe(
        ofType(logout),
        tap(action => {
          this.storageService.clear();
          this.router.navigate(['/login']);
        })
      ),
    {
      dispatch: false
    }
  );
  constructor(
    private readonly actions$: Actions,
    private readonly router: Router,
    @Inject(LOCAL_STORAGE) private readonly storageService: StorageService
  ) {}
}
