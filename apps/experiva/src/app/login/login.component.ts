import { Component, OnInit, Inject } from '@angular/core';
import { Store } from '@ngrx/store';
import { User } from '../shared/models/user';
import { login } from './login.actions';
import { AppState } from '../reducers';
import { StorageService } from '../core/services/storage/storage.service';
import { LocalStorageKeys } from '../shared/constants/storage-keys';
import { LOCAL_STORAGE } from '../core/services/storage/local-storage.token';

@Component({
  selector: 'experiva-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  constructor(
    @Inject(LOCAL_STORAGE) private readonly storageService: StorageService,
    private readonly store: Store<AppState>
  ) {}

  ngOnInit() {
    const user = this.storageService.get(LocalStorageKeys.user) as User;
    if (user) {
      this.store.dispatch(login({ user }));
    }
  }
  login(user: User) {
    this.store.dispatch(login({ user }));
  }
}
