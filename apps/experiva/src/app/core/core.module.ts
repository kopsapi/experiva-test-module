import { NgModule } from '@angular/core';
import { StorageModule } from './services/storage/storage.module';

@NgModule({
  declarations: [],
  imports: [StorageModule.forRoot(localStorage)],
  exports: [StorageModule]
})
export class CoreModule {}
