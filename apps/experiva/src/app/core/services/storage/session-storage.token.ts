import { InjectionToken } from '@angular/core';
import { StorageService } from './storage.service';
export const SESSION_STORAGE = new InjectionToken<StorageService>(
  'Session storage token'
);
