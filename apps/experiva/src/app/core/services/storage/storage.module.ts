import { NgModule, ModuleWithProviders } from '@angular/core';
import { StorageService } from './storage.service';
import { LOCAL_STORAGE } from './local-storage.token';
import { SESSION_STORAGE } from './session-storage.token';
@NgModule({})
export class StorageModule {
  static forRoot(store: Storage): ModuleWithProviders {
    return {
      ngModule: StorageModule,
      providers: [
        {
          provide: LOCAL_STORAGE,
          useValue: new StorageService(localStorage)
        },
        {
          provide: SESSION_STORAGE,
          useValue: new StorageService(sessionStorage)
        }
      ]
    };
  }
}
