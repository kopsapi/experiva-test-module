import { Injectable } from '@angular/core';

@Injectable()
export class StorageService {
  constructor(private readonly store: Storage) {}

  public set(key: string, value: object | string | number | Date | Array<any>) {
    this.store.setItem(this.encodeKey(key), this.encodeValue(value));
  }

  public get(key: string): object | string | number | Date | Array<any> {
    try {
      const localValue = this.store.getItem(this.encodeKey(key));
      if (!localValue) {
        return '';
      }
      return this.decodeValue(localValue);
    } catch (error) {
      return null;
    }
  }

  public delete(key: string) {
    this.store.removeItem(this.encodeKey(key));
  }

  public clear() {
    this.store.clear();
  }

  private encode(value: string) {
    return btoa(value);
  }
  private decode(value: string) {
    return atob(value);
  }
  private encodeKey(key: string) {
    return this.encode(key.toLowerCase());
  }
  private encodeValue(value: object | string | number | Date | Array<any>) {
    return this.encode(JSON.stringify(value));
  }
  private decodeValue(
    value: string
  ): object | string | number | Date | Array<any> {
    return JSON.parse(this.decode(value));
  }
}
