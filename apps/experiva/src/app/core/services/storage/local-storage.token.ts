import { InjectionToken } from '@angular/core';
import { StorageService } from './storage.service';
export const LOCAL_STORAGE = new InjectionToken<StorageService>(
  'Local storage token'
);
