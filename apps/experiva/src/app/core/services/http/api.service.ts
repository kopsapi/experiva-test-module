import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { CoreModule } from '../../core.module';

export const serverBase = environment.api;
export const apiBase = `${serverBase}v1/`;

export class Request {
  apiBase?: string;
  endpoint: string;
  routeParams?: { [key: string]: number | string | Date };
  queryParams?: { [key: string]: number | string | Date };
  body?: any;
  formData?: FormData;
  method: string;
}

export const appVersion = '1.0.1';

@Injectable({
  providedIn: CoreModule
})
export class ApiService {
  private apiBase: string;
  constructor(private http: HttpClient) {
    // this.apiBase = environment.production
    //   ? apiBase
    //   : 'https://811e9cb6-79ed-4f43-87e8-0c8f968e7e45.mock.pstmn.io/api/v1/';
    this.apiBase = apiBase;
  }

  private removeTrailingSlash(endpoint: string) {
    if (endpoint.endsWith('/')) {
      endpoint = endpoint.substr(0, endpoint.length - 1);
    }
    return endpoint;
  }

  private buildURL(request: Request): string {
    request.endpoint = this.removeTrailingSlash(request.endpoint);
    let route = this.removeTrailingSlash(
      `${request.apiBase ? request.apiBase : this.apiBase}${request.endpoint}`
    );
    if (request.routeParams) {
      for (const key in request.routeParams) {
        if (key in request.routeParams) {
          route = `${route}/${key}/${request.routeParams[key]}`;
        }
      }
    }
    let paramChar = route.indexOf('?') >= 0 ? '&' : '?';
    if (request.queryParams) {
      for (const key in request.queryParams) {
        if (request.queryParams.hasOwnProperty(key)) {
          route = `${route}${paramChar}${key}=${request.queryParams[key]}`;
          paramChar = '&';
        }
      }
    }
    route += `${paramChar}appVersion=${appVersion}`;
    return route;
  }

  sendRequest(request: Request): Observable<any> {
    const finalUrl = this.buildURL(request);

    return this.http
      .request(request.method, finalUrl, {
        body: request.body || request.formData
        // headers: {
        //     'school-id': `${schoolId}`
        // }
      })
      .pipe
      // timeout(30000)
      ();
  }
}
