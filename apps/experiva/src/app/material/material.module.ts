import { NgModule, ModuleWithProviders } from '@angular/core';
import { MatTabsModule } from '@angular/material/tabs';
import { MatChipsModule } from '@angular/material/chips';
import { MatDividerModule } from '@angular/material/divider';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MAT_DATE_FORMATS } from '@angular/material/core';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatListModule } from '@angular/material/list';
import { MatRadioModule } from '@angular/material/radio';
import { MatTableModule } from '@angular/material/table';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatSliderModule } from '@angular/material/slider';

const modules = [
  MatTabsModule,
  MatChipsModule,
  MatDividerModule,
  MatInputModule,
  MatSelectModule,
  MatDatepickerModule,
  MatMomentDateModule,
  MatIconModule,
  MatButtonModule,
  MatAutocompleteModule,
  MatCheckboxModule,
  MatRadioModule,
  MatBottomSheetModule,
  MatListModule,
  MatTableModule,
  MatButtonToggleModule,
  MatSliderModule
];
@NgModule({
  imports: modules,
  exports: modules
})
export class MaterialModule {
  static forFeature(): ModuleWithProviders {
    return {
      ngModule: MaterialModule,
      providers: [
        {
          provide: MAT_DATE_FORMATS,
          useValue: {
            display: {
              dateInput: 'DD MMM YYYY',
              monthYearLabel: 'MMM YYYY',
              dateA11yLabel: 'LL',
              monthYearA11yLabel: 'MMMM YYYY'
            }
          }
        }
      ]
    };
  }
}
