import { Pipe, PipeTransform, ChangeDetectorRef, OnDestroy } from "@angular/core";
import { Observable, Subject } from 'rxjs';
import { takeUntil, distinctUntilChanged, debounceTime } from 'rxjs/operators';

@Pipe({
    name: 'filter',
    pure: false
})
export class FilterPipe implements PipeTransform, OnDestroy {

    private readonly unsubscribe = new Subject();
    private valueStream: Observable<any>;
    private _latestValue: any = null;
    // constructor(private _ref: ChangeDetectorRef) { }

    ngOnDestroy(): void {
        this.unsubscribe.next();
    }
    transform(source: any[], valueStream: Observable<any>, filterFn?: (source: any[], match: any) => any[]) {

        if (source && valueStream) {
            if (!this.valueStream) {
                this._subscribeValueStream(valueStream);
            }
            if (valueStream !== this.valueStream) {
                this.ngOnDestroy();
                return this.transform(source, valueStream, filterFn);
            }
            if (filterFn) {
                return filterFn(source, this._latestValue)
            } else {
                return source.filter(el => el === this._latestValue);
            }
        } else {
            return source;
        }
    }

    _subscribeValueStream(stream: Observable<any>) {
        this.valueStream = stream;
        stream.pipe(takeUntil(this.unsubscribe), distinctUntilChanged(), debounceTime(500)).subscribe(value => {
            this._updateLatestValue(stream, value);
        })
    }

    private _updateLatestValue(async: Observable<any>, value: any): void {
        if (async === this.valueStream) {
            this._latestValue = value;
            // this._ref.markForCheck();
        }
    }

}