import * as moment from 'moment';

export function getTimeDiff(
  from: number | string | moment.Moment,
  to: number | string | moment.Moment
): string {
  // 2:8:15
  const milliseconds = moment(to).diff(moment(from));
  // const milliseconds = 202500000;
  let remainderMilliseconds = milliseconds;

  let divisor = 1000 * 60 * 60 * 24;
  const days = Math.floor(remainderMilliseconds / divisor);
  remainderMilliseconds = remainderMilliseconds % divisor;
  divisor = 1000 * 60 * 60;
  const hours = Math.floor(remainderMilliseconds / divisor);
  remainderMilliseconds = remainderMilliseconds % divisor;
  divisor = 1000 * 60;
  const minutes = Math.floor(remainderMilliseconds / divisor);
  remainderMilliseconds = remainderMilliseconds % divisor;
  return (
    (days ? `${days} day(s) ` : '') +
    (hours ? `${hours} hr ` : '') +
    (minutes ? `${minutes} mins` : '')
  );
}
