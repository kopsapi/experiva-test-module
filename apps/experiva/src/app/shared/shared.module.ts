import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FilterModule } from './pipes/filter/filter.module';
import { MaterialModule } from '../material/material.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FilterModule,
    MaterialModule.forFeature()
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FilterModule,
    MaterialModule
  ]
})
export class SharedModule { }
