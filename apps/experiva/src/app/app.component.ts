import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IGreeting } from '@experiva/api-interfaces';

@Component({
  selector: 'experiva-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  greetings$ = this.http.get<IGreeting>('/api/v1/greetings');
  constructor(private http: HttpClient) { }
}
