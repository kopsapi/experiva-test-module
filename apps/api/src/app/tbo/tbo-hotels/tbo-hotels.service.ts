import { Injectable } from '@nestjs/common';
import { TBOUsersService } from '../tbo-users/tbo-users.service';
import { TBOHotelSearchRequest } from '@experiva/api-interfaces';
import * as request from 'request';
@Injectable()
export class TBOHotelsService {
  constructor(private readonly tboUsers: TBOUsersService) {}
  async hotels(query: TBOHotelSearchRequest) {
    return new Promise(async (resolve, reject) => {
      const tboUser = await this.tboUsers.login();

      const options = {
        method: 'POST',
        url:
          'http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/GetHotelResult/',
        headers: {
          'Content-Type': 'application/json'
          // 'Accept-Encoding': 'gzip, deflate'
        },
        body: {
          ...query,
          TokenId: tboUser.TokenId
        },
        json: true
      };
      request(options, async (error, response, body) => {
        if (error) {
          return reject(error);
        }
        return resolve(body);
      });
    });
  }
}
