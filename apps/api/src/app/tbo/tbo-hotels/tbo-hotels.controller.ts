import { Controller, Get, Body, Req } from '@nestjs/common';
import { TBOHotelsService } from './tbo-hotels.service';
import { TBOHotelSearchRequest } from '@experiva/api-interfaces';

@Controller('tbo-hotels')
export class TBOHotelsController {
  constructor(private readonly hotelService: TBOHotelsService) {}
  @Get()
  async hotels(@Body() query: TBOHotelSearchRequest, @Req() req) {
    query.EndUserIp = req.clientIp;
    return await this.hotelService.hotels(query);
  }
}
