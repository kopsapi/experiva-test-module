import { Module } from '@nestjs/common';
import { TBOHotelsController } from './tbo-hotels.controller';
import { TBOHotelsService } from './tbo-hotels.service';
import { DBService } from '@experiva/data-access-layer';
import { TBOUsersModule } from '../tbo-users/tbo-users.module';

@Module({
  controllers: [TBOHotelsController],
  imports: [TBOUsersModule],
  providers: [TBOHotelsService, DBService]
})
export class TBOHotelsModule {}
