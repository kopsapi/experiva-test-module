import { Injectable } from '@nestjs/common';
import { DBService } from '@experiva/data-access-layer';
import {
  TBOUser,
  TBOFligthsQuery,
  ITBOFlightExtraCharge
} from '@experiva/api-interfaces';
import * as request from 'request';
import { TBOUsersService } from '../tbo-users/tbo-users.service';
import { TBOFlightExtraChargeModel } from './flight-extra-charge.model';
@Injectable()
export class TBOFilghtsService {
  constructor(
    private readonly tboUsers: TBOUsersService,
    private readonly marginService: DBService<ITBOFlightExtraCharge>
  ) {}

  // async all() {
  //     return ;
  // }
  async flights(query: TBOFligthsQuery) {
    return new Promise(async (resolve, reject) => {
      const tboUser = await this.tboUsers.login();

      const options = {
        method: 'POST',
        url:
          'http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/Search/',
        headers: {
          'Content-Type': 'application/json'
          // 'Accept-Encoding': 'gzip, deflate'
        },
        body: {
          ...query,
          TokenId: tboUser.TokenId
        },
        json: true
      };
      request(options, async (error, response, body) => {
        if (error) {
          return reject(error);
        }
        return resolve(body);
      });
    });
  }

  async margin(): Promise<ITBOFlightExtraCharge> {
    try {
      const extra = await this.marginService
        .model(TBOFlightExtraChargeModel)
        .findOne({});
      if (extra) {
        return extra;
      } else {
        const defaultExtra: ITBOFlightExtraCharge = {
          gst: 10,
          baseMargin: 3
        } as ITBOFlightExtraCharge;
        await this.marginService
          .model(TBOFlightExtraChargeModel)
          .create(defaultExtra);
        return defaultExtra;
      }
    } catch (error) {
      return {} as ITBOFlightExtraCharge;
    }
  }
}
