import { Controller, Get, Body, Post, Req } from '@nestjs/common';
import { TBOFilghtsService } from './tbo-flights.service';
import {
  TBOFligthsQuery,
  ITBOFlightExtraCharge
} from '@experiva/api-interfaces';

@Controller('tbo-flights')
export class TBOFlightsController {
  constructor(private readonly dataService: TBOFilghtsService) {}
  @Get('margin')
  async getMargin(): Promise<ITBOFlightExtraCharge> {
    return await this.dataService.margin();
  }
  @Post('one-way')
  getOneWayFlights(@Body() query: TBOFligthsQuery, @Req() req) {
    query.EndUserIp = req.clientIp;
    return this.dataService.flights(query);
  }
  @Post('return')
  getReturnFlights(@Body() query: TBOFligthsQuery, @Req() req) {
    query.EndUserIp = req.clientIp;
    return this.dataService.flights(query);
  }
  @Post('multi-stop')
  getMultiStopFlights(@Body() query: TBOFligthsQuery, @Req() req) {
    query.EndUserIp = req.clientIp;
    return this.dataService.flights(query);
  }
}
