import { Mongoose, model, Schema } from 'mongoose';
import { ITBOFlightExtraCharge } from '@experiva/api-interfaces';

const TBOFlightExtraCharge = new Schema(
  {
    baseMargin: Number,
    gst: Number
  },
  {
    timestamps: true
  }
);
export const TBOFlightExtraChargeModel = model<ITBOFlightExtraCharge>(
  'TBOFlightExtraCharge',
  TBOFlightExtraCharge
);
