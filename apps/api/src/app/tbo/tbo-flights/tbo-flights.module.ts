import { Module } from "@nestjs/common";
import { TBOFlightsController } from './tbo-flights.controller';
import { TBOFilghtsService } from './tbo-flights.service';
import { DBService } from '@experiva/data-access-layer';
import { TBOUsersModule } from '../tbo-users/tbo-users.module';

@Module({
    controllers: [TBOFlightsController],
    imports: [TBOUsersModule],
    providers: [TBOFilghtsService, DBService]
})
export class TBOFlightsModule {

}