import { Module } from "@nestjs/common";
import { TBOUsersController } from './tbo-users.controller';
import { TBOUsersService } from './tbo-users.service';
import { DBService } from '@experiva/data-access-layer';

@Module({
    controllers: [TBOUsersController],
    providers: [TBOUsersService, DBService],
    exports: [TBOUsersService]
})
export class TBOUsersModule {

}