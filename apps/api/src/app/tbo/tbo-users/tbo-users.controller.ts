import { Controller, Get } from '@nestjs/common';
import { TBOUsersService } from './tbo-users.service';

@Controller('tbo-users')
export class TBOUsersController {

    constructor(private readonly dataService: TBOUsersService) { }
    @Get('login')
    login() {
        return this.dataService.login();
    }
}