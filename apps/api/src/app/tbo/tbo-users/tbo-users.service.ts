import { Injectable } from '@nestjs/common';
import { DBService } from '@experiva/data-access-layer';
import { TBOUser } from '@experiva/api-interfaces';
import { TBOUserModel } from './tbo-user.model';
import * as request from 'request';
@Injectable()
export class TBOUsersService {
  constructor(private readonly dataService: DBService<TBOUser>) {}

  // async all() {
  //     return ;
  // }
  async login(): Promise<TBOUser> {
    return new Promise(async (resolve, reject) => {
      const today = new Date();
      today.setHours(0, 0, 0, 0);
      const tboUsers = await this.dataService
        .model(TBOUserModel)
        .find({ createdAt: { $gte: today } })
        .sort({ createdAt: 'desc' })
        .limit(1);
      if (tboUsers.length) {
        return resolve(tboUsers[0]);
      } else {
        const options = {
          method: 'POST',
          url:
            'http://api.tektravels.com/SharedServices/SharedData.svc/rest/Authenticate',
          headers: {
            'Content-Type': 'application/json'
          },
          body: {
            ClientId: 'ApiIntegrationNew',
            UserName: process.env.TBO_USERNAME,
            Password: process.env.TBO_PASSWORD,
            EndUserIp: '172.17.9.51'
          },
          json: true
        };
        request(options, async (error, response, body) => {
          if (error) {
            return reject(error);
          }
          const user = await this.dataService.model(TBOUserModel).create(body);
          return resolve(user);
        });
      }
    });
  }
}
