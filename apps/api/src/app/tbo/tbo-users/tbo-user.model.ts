import { Mongoose, model, Schema } from "mongoose";
import { TBOUser } from '@experiva/api-interfaces';

const TBOUserSchema = new Schema({
    Status: Number,
    TokenId: String,
    Error: {
        ErrorCode: Number,
        ErrorMessage: String
    },
    Member: {
        FirstName: String,
        LastName: String,
        Email: String,
        MemberId: Number,
        AgencyId: Number,
        LoginName: String,
        LoginDetails: String,
        isPrimaryAgent: Boolean
    }
}, {
    timestamps: true
});
export const TBOUserModel = model<TBOUser>('TBOUser', TBOUserSchema);