import { Module } from '@nestjs/common';
import { TBOUsersModule } from './tbo/tbo-users/tbo-users.module';
import * as dotenv from 'dotenv';
import { DBService } from '@experiva/data-access-layer';
import { TBOFlightsModule } from './tbo/tbo-flights/tbo-flights.module';
import { TBOHotelsModule } from './tbo/tbo-hotels/tbo-hotels.module';

dotenv.config({ path: `${__dirname}/assets/.env` });
@Module({
  imports: [TBOUsersModule, TBOFlightsModule, TBOHotelsModule],
  providers: [DBService]
})
export class AppModule {}
