/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 **/

import { NestFactory } from '@nestjs/core';

import { connect } from "mongoose";
import { AppModule } from './app/app.module';
import * as requestIp from "request-ip";
import * as cors from 'cors';
async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(requestIp.mw());
  app.use(cors({
    origin: true
  }));
  const globalPrefix = 'api/v1';
  app.setGlobalPrefix(globalPrefix);
  const port = process.env.PORT || 3333;
  await connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    dbName: 'experiva',
    useUnifiedTopology: true
  })
  await app.listen(port, '0.0.0.0', () => {
    console.log('Listening at http://localhost:' + port + '/' + globalPrefix);
  });
}

bootstrap();
