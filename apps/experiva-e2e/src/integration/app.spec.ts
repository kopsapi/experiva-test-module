import { getGreeting } from '../support/app.po';

describe('experiva', () => {
  beforeEach(() => cy.visit('/'));

  it('should display welcome message', () => {
    getGreeting().contains('Welcome to experiva!');
  });
});
